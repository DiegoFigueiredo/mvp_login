package com.example.dfigueiredo.testelandport.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by afernandes on 6/7/16.
 */
public class WSRequestt {

    @SerializedName("Entrada")
    private Object entrada;

    public Object getEntrada() {
        return entrada;
    }

    public void setEntrada(Object entrada) {
        this.entrada = entrada;
    }



}
